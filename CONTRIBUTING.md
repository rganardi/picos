PICOS Contribution Guide
========================

Filing a bug report or feature request
--------------------------------------

### Via GitLab

If you have a GitLab account, just head to PICOS' official
[issue tracker](https://gitlab.com/picos-api/picos/issues).

### Via mail

If you don't have a GitLab account but are fine with your issue being discussed
publicly, you can send a mail to `incoming+picos-api/picos@incoming.gitlab.com`.
This will create a GitLab issue automatically and you will receive any response
by mail.

### Via private mail

If you have a problem or idea that you don't want to talk publicly about, for
instance if you don't want to disclose the model you are having issues with,
you can write any (or all) of the current developers a mail. You can find the
addresses via the personal homepage links in the [README](README.md) file.

Submitting a code change
------------------------

The canoncial way to submit a code change is to

1. [fork the PICOS repository on GitLab](https://gitlab.com/picos-api/picos/forks/new),
2. clone your fork and make your application use it instead of your system's
   PICOS installation,
3. optionally create a local topic branch to work with,
4. modify the source and commit your changes, and lastly
5. make a pull request on GitLab so that we can test and merge your changes.

If you don't want to create a GitLab account, we are also happy to receive your
changes via mail as a patch created by `git patch`.

Implementing your solver
------------------------

If you want to implement support for a new solver, all you have to do is update
[solvers.py](picos/solvers.py) where applicable, and add a file called
`solver_<name>.py` in the same directory that contains your implementation. We
recommend that you read two or three of the existing solver implementations to
get an idea how things are done. If you want to know exactly how PICOS
communicates with your implementation, refer to the solver base class in
[solver.py](picos/solver.py).

Implementing a test case
------------------------

Production and unit test sets are implemented in the files in the
[tests](picos/tests) folder that start with `ptest_` and `utest_`, respectively.
If you want to add to our test pool, feel free to either extend these files or
create a new set, whatever is appropriate. Make sure that the tests you add are
not too computationally expensive since they are also run as part of our
continuous integration pipeline whenever a commit is pushed to GitLab.

Coding guidelines
-----------------

### Cleanup in progress

We are aiming to tidy up PICOS' codebase in the future to make it more robust
and easier to maintain and extend. That means that the cost of adding a new
feature often is to also refactor around the neighboring features. That being
said, you are encouraged to just rewrite a function that you feel does not look
so good, even if you initially just planned to add some text to it! :wink:

### Test coverage

Refactoring means stability in the long run, but can break features in the
short term. To prevent this from happening, we're happy to grow our set of
production and unit test cases. Hence, if you are running a local test to see
if your changes work, consider adding it as a permanent test case so that your
feature is protected from our clumsiness in the future.

